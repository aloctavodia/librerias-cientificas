# Librerías Científicas

Un listado de librerías científicas en el lenguaje de programación Python. 

Agradecemos si tienes algunas más para sugerir.

## Librerías Básicas

- [Jupyter Notebook](https://jupyter.org/) (Aplicación web que permite crear y compartir documentos que contienen codigo, ecuaciones, 
visualizaciones y texto)
- [Ipython](http://ipython.org/) (Shell interactivo contruido en Python)
- [Matplotlib](https://matplotlib.org/) (Visualización de datos)
- [Numpy](http://www.numpy.org/) (Soporte para Arrays y Matrices, además contiene funciones matemáticas de alto nivel)
- [Pandas](http://pandas.pydata.org/) (Manipulación y Analisis de datos)
- [Sciki-learn](http://scikit-learn.org/stable/) (Machine Learning)

## Estadística

- [PyMC3](https://docs.pymc.io/) (Estadística Bayesiana, Programación Probabilistica)
- [ArviZ](https://arviz-devs.github.io/arviz/) (Estadística Bayesiana, Visualización)
- [Bambi](https://github.com/bambinos/bambi) (Estadística Bayesiana)
- [statsmodels](https://www.statsmodels.org/stable/index.html) (Estadística frecuentista)

## Machine Learning

- [Keras](https://blog.keras.io) (Librería de alto nivel para Machine Learning orientado fuertemente a redes neuronales)
- [TensorFlow](https://www.tensorflow.org) (Deep learning)
- [PyTorch](https://pytorch.org/) (Deep learning)

## Procesado de imagenes

- [Scikit-Image](https://scikit-image.org)
- [SimpleITK](http://www.simpleitk.org) (Procesado de imágenes médicas)

## Matemática Simbolica

- [SimPy](http://www.sympy.org/en/index.html) 

## Astronomia

- [Astropy](http://www.astropy.org/) (El proyecto provee herramientas e infraestructura para facilitar la investigación para 
astronomos profesionales)
